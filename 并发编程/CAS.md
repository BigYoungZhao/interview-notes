[toc]

# 1、能聊聊你对CAS的理解以及其底层实现原理可以吗？

`**CAS**`全称是**CompareAndSwap**，即先比较再替换，CAS在底层的硬件级别保证了原子性，可以使用底层基于CAS实现的类完成一些线程同步的累加操作。例如`AtomicInteger`类。

CAS虽然高效的解决了原子操作，但仍然存在三大问题：

1. **ABA问题**：如果变量V初次读取的时候值是A，后来变成了B，然后又变成了A，你本来期望的值是第一个A才会设置新值，第二个A跟期望不符合，但却也能设置新值。针对这种情况，java并发包中提供了一个带有标记的原子引用类AtomicStampedReference，它可以通过控制变量值的版本号来保证CAS的正确性，比较两个值的引用是否一致，如果一致，才会设置新值。
2. **无限循环问题（自旋）**：看源码可知，Atomic类设置值的时候会进入一个无限循环，只要不成功，就会不停的循环再次尝试。在高并发时，如果大量线程频繁修改同一个值，可能会导致大量线程执行compareAndSet()方法时需要循环N次才能设置成功，即大量线程执行一个重复的空循环（自旋），造成大量开销。解决无线循环问题可以使用java8中的LongAdder，分段CAS和自动分段迁移。
3. **多变量原子问题**：只能保证一个共享变量的原子操作。一般的Atomic类，只能保证一个变量的原子性，但如果是多个变量呢？可以用AtomicReference，这个是封装自定义对象的，多个变量可以放一个自定义对象里，然后他会检查这个对象的引用是不是同一个。如果多个线程同时对一个对象变量的引用进行赋值，用AtomicReference的CAS操作可以解决并发冲突问题。 但是如果遇到ABA问题，AtomicReference就无能为力了，需要使用AtomicStampedReference来解决。

```java
import java.util.concurrent.atomic.AtomicInteger;

public class CASTest {

    AtomicInteger i = new AtomicInteger(0);

    public void increment(){
        i.incrementAndGet();
    }
}
```

![](http://processon.com/chart_image/60432efd079129476366ad42.png?_=1615733781318)
[toc]

# 1、说说synchronized关键字的底层原理是什么？



## 1.1、synchronized有三个作用

- **原子性**：指一个操作或者多个操作，要么全部执行并且执行的过程不会被任何因素打断，要么就都不执行。比较 **synchronized** 和 **volatile** ，它们俩特性上最大的区别就在于原子性，**volatile** 不具备原子性。

- **可见性**：指多个线程访问一个资源时，该资源的状态、值信息等对于其他线程都是可见的。`synchronized `和 `volatile `都具有可见性，其中 `synchronized `对一个类或对象加锁时，一个线程如果要访问该类或对象必须先获得它的锁，而这个锁的状态对于其他任何线程都是可见的，并且在释放锁之前会将对变量的修改刷新到主存当中，保证资源变量的可见性，如果某个线程占用了该锁，其他线程就必须在锁池中等待锁的释放。而 `volatile `的实现类似，被`volatile`修饰的变量，每当值需要修改时都会立即更新主存，主存是共享的，所有线程可见，所以确保了其他线程读取到的变量永远是最新值，保证可见性。

- **有序性**：指程序执行的顺序按照代码先后执行。`synchronized`和`volatile`都具有有序性，Java允许编译器和处理器对指令进行重排，但是指令重排并不会影响单线程的顺序，它影响的是多线程并发执行的顺序性。`synchronized`保证了每个时刻都只有一个线程访问同步代码块，也就确定了线程执行同步代码块是分先后顺序的，保证了有序性。

## 1.2、**synchronized的实现**

两种形式上锁：

1. 对方法上锁：通过flag标识**ACC_SYNCHRONIZED**识别
2. 对代码块上锁：通过**monitorenter**和**monitorexit**指令操作

```java
public class SynchronizedTest {
	// 锁方法
    public synchronized void method(){
        System.out.println("Hello world");
    }
    
    // 锁代码块
    public void codeBlock(){
        synchronized (this){
            System.out.println("Hello world");
        }
    }
}
```

### 1.2.1、对方法上锁

通过反编译可以看到在add方法的flags里面多了一个`ACC_SYNCHRONIZED`标志，这标志用来告诉JVM这是一个同步方法，在进入该方法之前先获取相应的锁，锁的计数器加1，方法结束后计数器-1，如果获取失败就阻塞住，知道该锁被释放。

![image.png](https://cdn.nlark.com/yuque/0/2021/png/1785555/1615002426651-f96c41c2-3cc0-4101-a0f3-f1b3a5658f85.png)

### 1.2.2、对代码块上锁

![image.png](https://cdn.nlark.com/yuque/0/2021/png/1785555/1615004643346-e545a27f-a764-4952-aefb-90ac417b242f.png)

为什么会有两个`monitorexit`呢？其实第二个`monitorexit`是来处理异常的，仔细看反编译的字节码，正常情况下第一个monitorexit之后会执行`goto`指令，而该指令转向的就是23行的`return`，也就是说正常情况下只会执行第一个`monitorexit`释放锁，然后返回。而如果在执行中发生了异常，第二个`monitorexit`就起作用了，它是由编译器自动生成的，在发生异常时处理异常然后释放掉锁。

**`synchronized`**底层是跟**`jvm`**指令和**`monitor`**有关系的

他里面的原理和思路大概是这样的，monitor 里面有一个计数器，从0开始的，如果一个线程要获取 monitor 的锁，就看看他的计数器是不是0，如果是0的话，那么说明没人获取到锁，他就可以加锁，然后对计数器加1.

你如果用到了`synchronized`关键字，在底层编译后的**`jvm`**指令中（可以通过 **`javap -verboss class文件`**），会有`monitorenter`**（执行加锁）**和 `monitorexit`**（执行解锁）**两个指令

这个monitor的锁是支持可重入锁的，如以下代码

```java
synchronized(myObject){
	// 一大堆代码
    synchronized(myObject){
    	// 一大堆代码
    }
}
```

如果一个线程第一次`synchronized`那里，获取到 myObject 对象的`monitor`的锁，计数器加1，然后第二次`synchronized`那里，会再次获取myObject对象的`monitor`的锁，这个就是可重入加锁了，然后计数器会再次加1，变成2。

这个时候，其他的线程在第一次 synchronized 那里，会发现说 myObject 对象的 monitor 锁的计数器是大于0的，意味着被其他线程加锁了，然后此时线程就会进入 block 阻塞状态，什么都干不了，就是等着获取锁。

接着如果出了 synchronized 修饰的代码片段的范围，在底层就会有一个 `monitorexit` 的指令，此时获取锁的线程就会对那个对象的 monitor 的计数器减1，如果有多次可重入加锁就会对应多次减1，直到最后，计数器为0。

然后后面 block 住阻塞的线程，会再次尝试获取锁，但是只有一个线程可以获取锁。
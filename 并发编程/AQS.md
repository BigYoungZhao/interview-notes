[toc]

# 1、你对JDK中的AQS理解吗？AQS的实现原理是什么？

**AQS**：Abstract Queue Synchronizer，抽象队列同步器

AQS有两种功能：

- **独占锁**：每次只能有一个线程持有锁
- 共享锁：允许多个线程同时获取锁，并发访问共享资源

 **AQS的原理**就是提供了一个volatile修饰的状态变量和一个双向的同步队列。提供模板方法对于独占锁和共享锁的获取和释放。

AQS队列内部维护的是一个FIFO(first-in-first-out,先进先出)的双向链表

## 1.1、一个线程释放锁后是如何去唤醒下一个线程的？

查看 `AbstractQueuedSynchronizer`1260行,`release`释放同步状态时;会判断同步双向队列中的头节点是否为空,同时头节点等待状态不为0(即不为初始化)时来唤醒头节点出队列；

头节点一般就是抢到了锁的线程的node，初始状态为0；什么时候不为0，一般是头结点的next节点，因为取不到锁，所以把头结点的状态改成signal（-1），表示自己需要通知；

总结：**AQS的核心**是被volatile修饰的state+双向链表Node+Condition单向队列。

## 1.2、ReentrantLock的原理？

![img](https://www.processon.com/view/link/60442ba6e401fd4f9cbb1e32.png)